---
title:      Learn By Play
layout:     page
permalink:  /learn-by-play/
---

![Logo de Learn By Play : une personne souriante et une manette de jeu](/assets/Learn_By_Play.png)

Le projet **Learn by Play** est un projet créé à l’initiative du **CELL** (le club de création de jeux du campus) et qui a pour but d’aider les jeunes en difficulté d’Evry à reprendre goût à l’apprentissage.
Pour ce faire, notre groupe composé de 9 étudiants va réaliser des ateliers de *jeux éducatifs* dans différents *établissements scolaires et parascolaires*.
Les jeux seront majoritairement au format physique car le format numérique n’est pas forcément possible à mettre en place dans tous les établissements.
En plus des jeux déjà existants, nous avons déjà pour projet avec la maison de quartier des Epinettes de créer un jeu éducatif avec les jeunes.
Ce jeu sera ensuite présenté à leurs parents lors d’une soirée kermesse.
Les activités proposées seront réalisées dans le cadre du programme scolaire du niveau des jeunes et ont pour objectif d’aider ces jeunes à progresser tout en comprenant l’importance de l’apprentissage.
