---
title: Calendrier
layout: page
permalink: calendrier.html
---

  * Amphi de rentrée
  * Présentation lors de l'initiation JdR 

  * Réunions les Jeudi de 16h à 18h


## Game Jams ##

  * Voir le calendrier [ici](https://itch.io/jams)
  * 10-13 Août 2018 : Ludum Dare 42
  * 29 Août 2018 : Mark Brown (Mirror Knight [ici](/realisations/))
  * 26 Octobre 2018 : [Pizza Jam](https://itch.io/jam/pizza-jam4)
  * 26-28 Novembre 2018 : Unijam
  * 30-31 Novembre : Ludum Dare 43  
  * 25-27 Janvier 2019 : Global Game Jam


## Festivals

  * Indie Cade (avec la PGW) (octobre)
  * Joutes de Saclay
  * Elfics


## Semaine du handicap ##

  * Escape game avec handicap
  * Première/seconde semaine de décembre

## Septembre ##

  * Nuit des associations

## Octobre ##

  * Formation *Game design* le jeudi 11 ou vendredi 12
  * Pizza Jam 12 Octobre

## Novembre ##

  * Escape game + jeu de piste avant la soirée MiNET

## Janvier

  * 30-31 escape game CJ

## Février ##

  * 26-27-28 escape game BDA Festiv'Art

