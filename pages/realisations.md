---
title: Réalisations
layout: page
permalink: /realisations/
---

## Mirror Knight

Jeu réalisé sous Unity pour la Game Maker's Toolkit Jam 2018

Vous pouvez l'essayer [ici](https://prez-cell.itch.io/mightyknight).


## [L'impasse mexicaine](/realisations/l-impasse-mexicaine/) ##

Jeu de plateau


## [A voté !](/realisations/a-vote) ##

Construisez vos propres règles !


## [Poulp'It](/realisations/poulpit/) ##

Moteur de jeu 2D réalisé en Java


## Lovum Darude ##

Élu meilleur de jeu de la décennie par un panel indépendant
