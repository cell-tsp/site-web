---
title: Oui, Seigneur des Ténèbres'Int
layout: page
permalink: /realisations/oui-seigneur-des-tenebres-int/
---

## Titre

  * Oui, chef de projet
  * Ténébrés'INT

  * le bus des vieux - «C'est plus ma guerre !»
  * IN&acT désapprouve !
  * la manette smash
  * la machoire
  * boris le panda
  * Lana le poulpe
  * la queue de billard
  * le calendrier IN&acT
  * l'oligarchie
  * les multiprises
  * Oui, Seigneur des ténèbres
  * une voiture
  * la voiture immatriculée CZ-846-ZK
  * les courses CJ
  * un pélican
  * l'opé moquette
  * la clef
  * le badge
  * l'INT
  * Bizmines
  * l'alcool
  * le car d'étranger
  * la nuit de sommeil (RIP)
  * les lapins virtuels
  * le quart d'heure de soleil annuel
  * le plagiat
  * steak-frite
  * les 800 capotes
  * le billard
  * le distributeur MAISEL
  * les courses CJ
  * Roi Dadidou
  * la mailing-list des préz'
  * le verre d'eau - l'eau ça rouille
  * le lavabo - ilébo
  * le robot d'INTech
  * pour des raisons de sécurité
  * le shotgun
  * le chat mort dans le local
  * SUUS
  * du gaffeur
  * le chien - chieeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeen
  * le chaton abandonné
  * le monopoly

## Lieux

  * La butte aux lapins
  * La salle SSP
  * la MAISEL
  * le plafond / cuisine U4
  * l'amphi étoile
  * le potager
  * U6
  * le bar
  * le gymnase
  * la réserve
  * le bâtiment G
  * BL006
  * réserve en bas du BDE Tijet
  * les témoins de jéhovah
  * le pôle multimédia
  * le terrain de voile
  * le ritz
  * la médiathèque
  * le G20

## Personnes

  * Babeth
  * Rémi Bachelet
  * Randal Douc
  * Monfrini et son chien
  * FF
  * le joueur de flute
  * le respo frigo
  * le technicien MAISEL
  * le manager
  * l'ingénieur
  * claire lecocq
  * le bachelor
  * Quentin
  * cet abruti de listeux
  * les pompoms
  * le geek boutonneux
  * la gendarmerie nationale
  * le consanguin CJ IN&acT
  * le noob rageux smash
  * le respo soirées
  * le vice-président
  * le directeur de TSP
  * le respo com'
  * 1A
  * 2A
  * 3A
  * le dinosaure
  * le 1B - «Je suis pas 1B, mais 1A'»
  * Torinne Cruche
  * le trésorier qui dit non
  * le prèz CELL

## Clubs

  * BDS (ASINT)
  * Le club jeu
  * XTrem
  * InTimes
  * le club de la wifi
  * inéakt
  * le club improbable
  * la secte Intech
  * le club techno
  * Uberin't
  * cheval'int
  * Nobod'int
  * le club inexistant
  * le groupe Campus TMSP
  * la conv messenger
  * le club jouet
  * Sprint
  * l'amphi de présentation
  * Aiesecte
  * fragilint
  * apocalypse
  * le club de danse
  * la comédie musicale
  * le club finance

## Événements

  * le cabaret
  * le dépistage In&Act
  * la campagne BDE
  * la panne de réseau
  * les alarmes incendie
  * le WEI
  * une soirée
  * l'amphi dolph'int
  * la croisière dolph'int
  * le rebranding
  * le greuh

## Cours

  * CF2 de math
  * CF6 de signal
  * CF18
  * le GATE pipo
  * les cours de SH
  * le mail rappelant le devoir à rendre dans 10 minutes
  * l'amphi vide
  * l'hyperfréquence
  * les pougnes
  * les poly d'élec
  * le tp à distance
  * moodle
  * le projet Buddy
  * le dossier de développement durable
  * le membre pipo
  * le service des stages
  * la semaine image

## Les originales

  * la taverne
  * les succubes
  * l'herbe aux songes
  * Gona l'arnaqueur
  * le vieux rapace
  * le parchemin mystérieux
  * Rigor Mortis
