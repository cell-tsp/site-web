---
title: Poulp'It
layout: page
permalink: /realisations/poulpit/
---

**Poulp'It** est un moteur de jeu en 2D.
Il est codé en Java.
Le code source est disponible sur [Github](https://github.com/matthias4217/Poulp-it).
