# TODO

  * [ ] Automatiser le déploiement des changements du site web
  * [ ] Mettre à jour la VM
  * [ ] Définir les bonnes pratiques pour push/pull sur le git du site web
  (push sur master à l'arrache ou branches et MR)
  * [ ] Theme
    * [ ] Choose a theme (see [here](http://jekyllthemes.org/))
    * [ ] Merge theme with existing project (⛧)