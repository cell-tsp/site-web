---
layout: page
title: About
permalink: /about/
---

Le **CELL** est un club dépendant de [MiNET](https://www.minet.net).

[Statuts](/organigramme.html)

Pour nous contacter :

  * [Page Facebook](https://www.facebook.com/Club-Cr%C3%A9ation-de-Jeux-PoulpIT-1613053882063320/)
  * [Discord](https://discord.gg/aerqXpM)
